package com.infrastructure;

public class DBInfo {
	static public enum DBDriverType {
		OracleDB,
		SQLite,
		Postgres
	}

	static public final DBDriverType DB_TYPE = DBDriverType.SQLite;
	static public final String DB_URI = "";
	static public final String DB_USER = "";
	static public final String DB_PASSWORD = "";
}
