
package com.adaptor.controller.rest;
import java.util.ArrayList;
import java.util.Date;

import com.entity.model.todo.TodoEntity;

public class TodoController {

	public TodoEntity getTodo() {
		return TodoEntity.build(1,"明日も頑張る",new Date(),false);
	}
	public ArrayList<TodoEntity> getTodos() {
		final ArrayList<TodoEntity> res = new ArrayList<TodoEntity>();
		res.add(TodoEntity.build(1,"明日も頑張る",new Date(), false));
		res.add(TodoEntity.build(1,"明後日も頑張る",new Date(), false));
		return res;
	}
}
