package com.adaptor.presenter.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adaptor.controller.rest.TodoController;
import com.entity.model.todo.TodoEntity;

@Controller
@ResponseBody
public class Todo {


	@RequestMapping(value = "/todo", method = { RequestMethod.GET})
	public ResponseEntity<ArrayList<TodoEntity>> Index(@RequestParam("Key") Optional<String> key){
		TodoController todoController = new TodoController();
		ArrayList<TodoEntity> todoEntity = null;
		todoEntity = todoController.getTodos();

		return ResponseEntity.ok(todoEntity);// ←ポイントです！！
	}
}
