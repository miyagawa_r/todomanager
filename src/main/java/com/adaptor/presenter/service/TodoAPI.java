package com.adaptor.presenter.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoAPI {

	public static void main(String[] args) {
		SpringApplication.run(TodoAPI.class, args);
	}

}
