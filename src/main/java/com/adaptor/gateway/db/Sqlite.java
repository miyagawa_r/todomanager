package com.adaptor.gateway.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.entity.model.todo.TodoEntity;
import com.infrastructure.DBInfo;

public class Sqlite {

	public ArrayList<TodoEntity> getAll() throws SQLException{
	    final String SQL = "select * from schedule;";
	    try(Connection conn =
	            DriverManager.getConnection(DBInfo.DB_URI);
	        PreparedStatement ps = conn.prepareStatement(SQL)){

	        try(ResultSet rs = ps.executeQuery()){
	        	ArrayList<TodoEntity> result = new ArrayList<TodoEntity>();
	            while (rs.next()) {
	            	TodoEntity todo = TodoEntity.build(
	            		rs.getInt("id"),
	                	rs.getString("contents"),
	                    rs.getTimestamp("deadLine"),
	                    rs.getBoolean("completed")
	                );
	            	result.add(todo);
	            }
	            return result;
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        System.out.println("処理が完了しました");
	    }
		throw new SQLException("Error");
	}
}
