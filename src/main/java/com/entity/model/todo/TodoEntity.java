package com.entity.model.todo;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TodoEntity implements Serializable {

    private int ID;
	private String contents;
	private Date deadLine;
	private boolean completed;

    public TodoEntity(int id, String name, Date deadLine) {
    	this.ID = id;
    	this.contents = name;
    	this.deadLine = deadLine;
    	this.completed = false;
    }

    public TodoEntity(int id, String name, Date deadLine, boolean completed) {
    	this.ID = id;
    	this.contents = name;
    	this.deadLine = deadLine;
    	this.completed = completed;
    }

    public int getID() {
    	return this.ID;
    }

    public String getContents() {
    	return this.contents;
    }

    public Date getDeadLine() {
    	return this.deadLine;
    }

    public boolean isCompleted() {
    	return this.completed;
    }

    public String toJSON() throws JsonProcessingException {
    	ObjectMapper m = new ObjectMapper();
    	return m.writeValueAsString(this);
    }
    static public TodoEntity build(int id, String contents, Date deadLine, boolean completed) {
		return new TodoEntity(id,contents,deadLine,completed);
	}
}
