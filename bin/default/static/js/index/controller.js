
var MySchedule = MySchedule || {};

MySchedule.controller = {

    storage : Storage,

    bind : function(){
        MySchedule.view.addPostAction(this.postNewSchedule);
        MySchedule.view.addCheckBoxAction(this.updateCompletely);
        MySchedule.view.addDeleteButtonAction(this.deleteSchedule)
    },

    postNewSchedule : function(id, contents, deadLine){
        var contents = MySchedule.view.getNewContents();
        var deadLine = MySchedule.view.getDeadLine();
        var id = MySchedule.view.getID()
        if(!(contents && deadLine)) return ;
        var schedule = MySchedule.model.schedule.createNewSchedule(id,contents,deadLine,false);
        Storage.putJSON(id,schedule);
        MySchedule.view.addListRow(schedule);
    },

    deleteSchedule : function(e){
        var id = e.target.id;
        Storage.remove(id);
        MySchedule.view.removeListRow(id);
    },

    getSchedule: function(){
        var id = 0;
        var list = []
        restAPI.todo.get().then((data) => {
        	list.push(data);
            while(id < 100){
                id ++;
                var schedule = Storage.popJSON(id);
                if(!schedule) continue;
                list.push(schedule)
            };
            MySchedule.view.addList(list);
        })
    },

    updateCompletely: function(e, status){
        var id = e.target.id;
        var status = e.target.checked;
        var schedule = Storage.popJSON(id);
        schedule.completely = status;
        Storage.putJSON(id, schedule);
    }
};

$(function () {
    MySchedule.controller.bind();
    MySchedule.controller.getSchedule();
});