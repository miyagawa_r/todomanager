
var MySchedule = MySchedule || {};

MySchedule.model = {
    schedule : {
        createNewSchedule : function(id, contents, deadLine, completely){
            return ({
                id: id,
                contents : contents,
                deadLine : deadLine,
                complete : completely
            })
        }
    }
};
